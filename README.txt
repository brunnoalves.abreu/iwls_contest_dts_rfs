This solution uses the scikit-learn from Python to run Decision Trees and Random Forest classifiers with varying parameters. We apply different mechanisms of feature selection before the training. Additionally, we run a neural network, and based on the most significant features (the ones whose weight were the highest) given by the NN, we run several different functions with combinations of the 4 most significant features.

INSTRUCTIONS TO RUN:
- Insert all the python scripts to a folder;
- Insert the Benchmarks folder to the same folder where the scripts are in;
- Insert the ABC executable (named abc) inside this same folder where the scripts are in;
- Run the run_all.sh script (sh run_all.sh);
- Best AIG for each example are in the AIGS_BEST folder.
